#!/usr/bin/env bun

//require('dotenv').config({ silent: true })

let cli = require('./cli')
let args = cli.getArgv()

cli.printHeader()
let mainCommand = args['_'][0]
if (!mainCommand) {
    console.log('No command detected, launching server')
    require('./server')
        .start(args)
        .catch(function onServerError(err) {
            console.log(err)
        })
} else {
    cli.execute(mainCommand, args)
}