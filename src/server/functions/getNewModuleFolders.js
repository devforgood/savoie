const fs = require('fs').promises;
const isDirectory = async path => (await fs.stat(path)).isDirectory()

module.exports = app => {
    /**
     * @param {Array} modules List of DB modules [{title}] (REQUIRED)
     */
    return async function getNewModuleFolders(modules) {
        var debug = require('debug')(`app:getNewModuleFolders`)
        const appsPath = `${process.cwd()}/apps`
        let filesInAppsFolder = await fs.readdir(appsPath)
        filesInAppsFolder = filesInAppsFolder.filter(n => !modules.some(m => m.title === n))
        filesInAppsFolder = await Promise.all(filesInAppsFolder.map(async (n) => {
            return {
                n,
                isDir: await isDirectory(appsPath + '/' + n)
            }
        }))
        filesInAppsFolder = filesInAppsFolder.filter(f => f.isDir && f.n !== 'default')
        const result = filesInAppsFolder.map(f => f.n)
        debug({
            result
        })
        return result
    }
}