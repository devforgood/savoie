
module.exports = app => {
    return async function hashPwd(password) {
        var debug = require('debug')(`app:hashPwd`)
        const bcryptHash = await Bun.password.hash(password, {
            algorithm: "bcrypt",
            cost: 4, // number between 4-31
        });
        return bcryptHash
    }
}