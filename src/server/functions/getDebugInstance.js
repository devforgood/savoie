let moment = require('moment-timezone')
module.exports = app =>
    function getDebugInstance(name, level = 4) {
        let levelLabel = {
            1: 'ERROR'.red,
            2: 'WARN'.yellow,
            3: 'INFO'.blue,
            4: 'DEBUG'.grey
        }
        let _level = parseInt(process.env.DEBUG_LEVEL) || 4
        if (level > _level) {
            return () => {}
        } else {
            return require('debug')(
                    `${`app:${name.toUpperCase().blue}`} ${levelLabel[level]} ${
          `${moment(Date.now()).format('DD-MM-YY hh:mm:ss')}`.white
        }`
      )
    }
  }