module.exports = app => {
  return async function editUserPassword (email, oldPassword, newPassword) {
    var debug = require('debug')(`app:editUserPassword ${`${Date.now()}`.white}`)
    hashedPassword = await app.hashPwd(newPassword)
    let dbConnection = await app.getMysqlConnection()
    // WIP: oldPassword check
    let result = await dbConnection.execute(
      `UPDATE users SET password = ? WHERE email = ?`,
      [hashedPassword, email]
    )
    dbConnection.release()
    debug({
      email //, oldPassword, newPassword
    })
  }
}