

module.exports = app => {
  return async function loadModules() {

    var debug = app.getDebugInstance('modules')

    async function LoadSingleModule(module) {

      /*debug('load-single-module',{
        module
      })*/

      let name = module.title
        .split(' ')
        .join('-')
        .toLowerCase()

      try {
        module.name = name
        module.dbName = module.db_name
        let requirePath = require('path').join(process.cwd(), 'apps', module.name)
        module.basePath = requirePath
        app.builder.distFolder = `dist/${name}`
        app.builder.cwd = module.basePath
        module.getPath = p => {
          if (!p) return requirePath
          else return require('path').join(requirePath, p)
        }
        module.getRouteName = p => {
          if (!p) return `/` + module.name
          return require('path').join('/' + module.name, p)
        }
        // debug(`${module.title} loading... ${name}`)
        let moduleResponse = require(requirePath)(app, module)
        if (moduleResponse instanceof Promise) {
          moduleResponse = await moduleResponse
        }
        debug(`${module.title} loaded as ${name}`)
      } catch (err) {
        if (err?.stack?.indexOf('Cannot find module') !== -1) {
          return;
        }
        debug(`${module.title} failed to load`.red, {
          err: err.stack
        })
      }
      app.builder.distFolder = ''
      app.builder.cwd = ''
    }

    let conn = await app.getMysqlConnection()
    let isProduction = process.env.NODE_ENV === 'production'
    let [modules, fields] = await conn.execute(
      `SELECT * FROM modules ${isProduction ? `WHERE enabled = 1` : ``}`,
      []
    )
    conn.release()



    const newModules = await app.getNewModuleFolders(modules)
    if (newModules.length > 0) {
      
      debug(`
${'==='.green}

The following modules will not be loaded
${newModules.join(', ').green}

Configure them first using
${'bun savoie addModule --n [folderName]'.green}

${'==='.green}
      `.green)
      
    }


    if (!modules.some(m => m.name === 'default' || m.title === 'default')) {
      modules.push({
        name: 'default',
        title: 'default'
      })
    }

    let promises = modules.map(module => {
      return () => LoadSingleModule(module)
    })
    const sequential = require('promise-sequential')
    await sequential(promises)
    debug(
      `${modules.length} modules loaded from ${process.env.MYSQL_DATABASE
      }.modules`
    )
  }
}