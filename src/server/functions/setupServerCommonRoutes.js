module.exports = app => {
    const createDebug = require('./getDebugInstance')(app)
    const debug = createDebug('common', 4)

    return async function setupServerCommonRoutes() {
        ;
        (() => {
            const debug = createDebug('request')
            app.use((req, res, next) => {
                debug(`${req.method} ${req.url}`)
                next()
            })
        })()

        app.use((req, res, next) => {
            app.functions.forEach(
                functionName => (req[functionName] = app[functionName])
            )
            next()
        })

        app.get(
            '/analytics.js',
            app.webpackMiddleware({
                entry: require('path').join(process.cwd(), 'src/js/analytics.js')
            })
        )

        app.get(
            '/commonHeader.js',
            app.webpackMiddleware({
                entry: require('path').join(process.cwd(), 'src/js/commonHeader.js')
            })
        )

        app.get(
            '/feedbackButton.js',
            app.webpackMiddleware({
                entry: require('path').join(process.cwd(), 'src/js/feedbackButton.js')
            })
        )
        app.get('/health', (req, res) => res.send('alive'))
        app.get('/version', async (req, res) =>
            res.send(
                JSON.parse(
                    (await require('sander').readFile(
                        require('path').join(process.cwd(), 'package.json')
                    )).toString('utf-8')
                ).version
            )
        )

        const express = require('express')
        app.use('/', express.static('dist'))
        app.use('/static', express.static('src/static'))



        let routes = app._router.stack.map(l => ({
            name: l.name,
            isRoot: l.regexp.test('/')
        }))
        if (!routes.some(r => r.name === 'bound dispatch' && r.isRoot)) {
            app.get('/', (req, res) => res.send(`
            SavoieTech framework
            <br>
            It seems like there is no app using the root path. Check your apps folder.
            <br>
            If you want to redirect to an app relative path, configure "SAVOIE_ROOT_DEFAULT_REDIRECT"
            `))
        }


    }
}