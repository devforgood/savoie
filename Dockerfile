FROM node:10.16.0
MAINTAINER Javier Leandro Arancibia <arancibiajav@gmail.com>
WORKDIR /app
COPY . .
RUN npm ci
ENTRYPOINT ["npm", "run","start"]